import { fetch } from '../api/budget';

// States

const defaultState = {
  destinations: []
};

// Getters

const getters = {

  allDestinations(state) {
    return state.destinations;
  }

};

// Mutations

const mutations = {

  getAllDestinations(state, destinations) {
    state.destinations = destinations
  }

};

// Actions

const actions = {

  getAllDestinations(context) {

    fetch('destinations')
      .then(resolve => {
        context.commit('getAllDestinations', resolve)
      })
      .catch(error => {
        console.log(error)
      })
  }

};

export default {
  state: defaultState,
  getters,
  mutations,
  actions
}
