import { fetch } from '../api/budget';

// States
const defaultState = {
  hotels: [], hotel: {}
};

// Getters

const getters = {

  allHotels(state) {
    return state.hotels;
  },
  hotel(state) {
    return state.hotel
  }

};

// Mutations

const mutations = {

  getAllHotels(state, hotels) {
    state.hotels = hotels
  },
  getHotel(state, hotel) {
   state.hotel = hotel
  }

};

// Actions

const actions = {

  getAllHotels(context) {

    fetch('hotels')
      .then(resolve => {
        context.commit('getAllHotels', resolve)
      })
      .catch(error => {
        console.log(error)
      })
  },

  getHotel(context, payload) {

    let hotel = {};
    fetch('hotels').then((resolve) => {

      hotel = resolve.find(h => (h.id === payload.hotelId));
      context.commit('getHotel', hotel);

    }).catch(error => {
      console.log(error)
    })

  }

};

export default {
  state: defaultState,
  getters,
  mutations,
  actions
}
