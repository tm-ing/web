import Vue from 'vue';
import Vuex from 'vuex';
import post from './post';
import users from './users';
import photos from './photos';
import hotels from './hotels';
import rooms from './rooms'
import destinations from './destinations'

Vue.use(Vuex);

export const store = new Vuex.Store({
  modules: { hotels, rooms, destinations }
});
