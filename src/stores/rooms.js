import { fetch } from '../api/budget';
import { tConvert } from '../utils/time';

// States
const defaultState = {
  rooms: [], room: {}
};

// Getters
const getters = {

  allRoomsHotel(state) {
    return state.rooms
  },
  roomHotel(state) {
    return state.room
  }

};

// Mutations
const mutations = {

  getAllRoomsHotel(state, hotel) {
    state.rooms = hotel.rooms;
  },

  getRoomHotel(state, room) {
    state.room = room
  }

};

// Actions
const actions = {

  getAllRooms(context, payload) {

    let hotel = {};
    fetch('hotels')
      .then((resolve) => {

        hotel = resolve.find(x => x.id === payload.hotelId);

        context.commit('getAllRoomsHotel', hotel)
      })
      .catch(error => {
        console.log(error)
      })
  },

  getRoom(context, payload) {

    let room = {};
    fetch('hotels')
      .then((resolve) => {

        room = resolve.find(h => (h.id === payload.hotelId))
          .rooms.find(r => (r.room_id === payload.roomId));

        let entryFormatted = tConvert(room.entry);
        let departureFormatted = tConvert(room.departure);

        room.entry = entryFormatted;
        room.departure = departureFormatted;

        context.commit('getRoomHotel', room);

      }).catch(error => {
        console.log(error)
      })

  }

};

export default {
  state: defaultState,
  getters,
  mutations,
  actions
}

