import { fetch } from '../api/index';

const defaultState = {
    posts: []
}

// Getters

const getters = {

    allPosts(state) {
        return state.posts;
    }
}

// Mutations

const mutations = {

    getAllPosts(state, posts) {
        state.posts = posts;
    }
}

// Actions

const actions = {

    getAllPosts(context, posts) {

        fetch('posts')
            .then(resolve => {
                context.commit('getAllPosts', resolve)
            })
            .catch(error => {
                console.log(error)
            })
    }
}

export default ({
    state: defaultState,
    getters,
    mutations,
    actions
});
