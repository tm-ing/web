import { fetch } from '../api/';

const defaultState = {
    photos: []
}

// Getters

const getters = {

    allPhotos(state) {
        return state.photos;
    }
}

// Mutations

const mutations = {

    getAllPhotos(state, photos) {
        state.photos = photos;
    }
}

// Actions

const actions = {

    getAllPhotos(context, photos) {

        fetch('photos')
            .then(resolve => {
                context.commit('getAllPhotos', resolve)
            })
            .catch(error => {
                console.log(error)
            })
    }
}

export default ({
    state: defaultState,
    getters,
    mutations,
    actions
});
