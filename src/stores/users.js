
// States

const defaultState = {
    registrations: [],
    users: [
        { id: 1, name: 'Sansa Stark', registered: false},
        { id: 2, name: 'Arya Stark', registered: false},
        { id: 3, name: 'Daenerys Targeryan', registered: false},
        { id: 4, name: 'John Snow', registered: false},
        { id: 5, name: 'Tyrion Lannister', registered: false},
        { id: 6, name: 'Cersei Lannister', registered: false},
        { id: 7, name: 'Yara Greyjoy', registered: false},
    ],
}

// Getters

const getters = {
    unregisteredUsers(state) {
        return state.users.filter(user => {
            return !user.registered;
        });
    },
    registrations(state) {
        return state.registrations;
    },
    totalRegistrations(state) {
        return state.registrations.length;
    }
}

// Mutations

const mutations = {
    register(state, userId) {

        const date = new Date();

        const user = state.users.find(user => {
            return user.id == userId;
        });

        user.registered = true;

        const registration = {
            userId: userId,
            name: user.name,
            date: date.getMonth() + '/' + date.getDay()
        }

        state.registrations.push(registration);
    },
    unregister(state, payload) {

        const user = state.users.find(user => {
            return user.id == payload.userId;
        });

        user.registered = false;

        const registration = state.registrations.find(registration => {
            return registration.userId == payload.userId
        });

        state.registrations.splice(state.registrations.indexOf(registration), 1);
    }
}

// Actions 

const actions = {
    register(context, userId) {
        context.commit('register', userId);
    },
    unregister({ commit }, payload) {
        commit('unregister', payload);
    }
}

export default {
    state: defaultState,
    getters,
    mutations,
    actions
}