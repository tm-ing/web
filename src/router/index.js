import Vue from 'vue';
import VueRouter from 'vue-router'
import HomeView from '../views/Home.vue'
import RegisterView from '../views/Register.vue'
import UsersView from '../views/Users.vue'
import PostsView from '../views/Posts.vue'
import PhotosView from '../views/Photos.vue'
import HotelView from '../views/Hotel.vue'
import RoomView from '../views/Room.vue'

Vue.use(VueRouter);

const routes = [
  { path: '/', component: HomeView },
  { path: '/hotel/:id', component: HotelView },
  { path: '/hotel/:hotelId/room/:roomId', component: RoomView },
  { path: '/registers', component: RegisterView },
  { path: '/users', component: UsersView },
  { path: '/posts', component: PostsView},
  { path: '/photos', component: PhotosView }
];

export default new VueRouter({ routes })
