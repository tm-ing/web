import Vue from 'vue'
import App from './App.vue'
import { store } from './stores/index';
import router from './router/index';
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue);

new Vue({ el: '#app', store, router, render: h => h(App) });
