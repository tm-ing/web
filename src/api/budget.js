import Vue from 'vue';
import VueResource from 'vue-resource';

Vue.use(VueResource);

const API_URL = 'http://localhost:3500/';

const fetch = (entity, routeParams = []) => {

  let _routeParams = (routeParams === [])? '/' +  routeParams.join('/') : '';

  const url = API_URL + entity + _routeParams;

  return new Promise((resolve, reject) => {

    Vue.http.get(url)
      .then((response) => {
        resolve(response.data)
      }, (error) => {
        reject(error)
      })
  })

};

export { fetch };
