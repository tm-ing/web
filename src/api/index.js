import Vue from 'vue';
import VueResource from 'vue-resource';

Vue.use(VueResource);

const API_URL = 'https://jsonplaceholder.typicode.com/'

const fetch = (entity, routeParams = []) => {

    const url = API_URL + entity + '/' +  routeParams.join('/')

    return new Promise((resolve, reject) => {

        Vue.http.get(url)
            .then((response) => {
                resolve(response.data)
            }, (error) => {
                reject(error)
            })
    })
}

// const fetchPosts = new Promise((resolve, reject) => {

//     Vue.http.get('https://jsonplaceholder.typicode.com/posts')
//         .then((response) => {
//             resolve(response.data)
//         }, (error) => {
//             reject(error)
//         })
// })

// const fetchPhotos = new Promise((resolve, reject) => {

//     Vue.http.get('https://jsonplaceholder.typicode.com/photos')
//         .then((response) => {
//             resolve(response.data)
//         }, (error) => {
//             reject(error)
//         })
// })

export { fetch };
